package model

import (
    "database/sql"
    _ "github.com/mattn/go-sqlite3"

)

const SQLITE3 = "sqlite3"
const DBFILE = "./test_file.sql"

type Item struct {
    Id      int    `json:"id"`
    Name    string `json:"name"`
    Price   int    `json:"price"`
}

func CreateTable() {
    dbconn, _ := sql.Open(SQLITE3, DBFILE)
    stmt, _ := dbconn.Prepare("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name TEXT, price INT)")
    stmt.Exec()
}

func InsertItem(item Item) (err error) {
    dbconn, err := sql.Open(SQLITE3, DBFILE)
    stmt, err := dbconn.Prepare("INSERT INTO items(id, name, price) VALUES (?, ?, ?)")
    stmt.Exec(item.Id, item.Name, item.Price)
    return err
}

func GetItemById(id int) (Item, error) {
    var item Item
    dbconn, _ := sql.Open(SQLITE3, DBFILE)
    stmt := "SELECT id, name, price FROM items WHERE id=$1"
    row := dbconn.QueryRow(stmt, id)
    err := row.Scan(&item.Id, &item.Name, &item.Price)

    return item, err
}
