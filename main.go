package main

import (
    "bitbucket.org/bagaspurwas/go-test-snyk/model"
    "encoding/json"
    "log"
    "net/http"
)

func InsertHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == "GET" {
        http.Error(w, "GET not supported", 405)
    }
    if r.Method == "POST" {
        decoder := json.NewDecoder(r.Body)
        var item model.Item
        err := decoder.Decode(&item)
        if err != nil  {
            http.Error(w, "Error parsing Body", 400)
        }
        err = model.InsertItem(item)
        if err != nil {
            http.Error(w, "Database operation error", 500)
        }
    }
}

func main() {
    log.Print("Test\n")
    http.HandleFunc("/insert_item", InsertHandler)
    log.Fatal(http.ListenAndServe(":8082", nil))

}
